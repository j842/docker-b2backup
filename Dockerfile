# server to perform backup and restore of a path or a docker volume container
#FROM phusion/baseimage
FROM debian:jessie
MAINTAINER j842

RUN apt-get update ; apt-get install -y \
   build-essential \
   gnupg lftp \
   python \
   python-setuptools \
   python-dev \
   librsync-dev \
   python-paramiko \
   python-pycryptopp \
   python-lockfile \
   python-boto \
   librsync1
    
ADD ["./assets","/"]
# commit 1166 of duplicity fixes critical b2 bugs, so we use a tgz of that. Will switch to next official release when out.
RUN /bin/bash -c 'cd /opt; tar zxvf duplicity.tgz'
RUN /bin/bash -c 'cd /opt/duplicity* ; /usr/bin/python setup.py install'

VOLUME ["/data"]
CMD ["/bin/backup"]
