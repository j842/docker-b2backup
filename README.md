# B2 Backups #

Docker container to backup a path on host or a docker volume container to [backblaze b2](https://www.backblaze.com/b2/cloud-storage.html) using the latest [duplicity](http://duplicity.nongnu.org/). Also supports restore to an empty target.

Always uses backup diffs (no forced full backups) - you can change B2_DIR to force a full backup. Backups are encrypted. 

Environment variables that need to be set:

Env Var         | Details
----------------|-------------------
B2_ACCOUNT_ID   | Account ID provided by Backblaze (get to it from the Buckets tab)
B2_APP_KEY      | Application Key provided by Backblaze
B2_BUCKET       | Name of the bucket. Must be created in Backblaze before use.
B2_DIR          | A path within the bucket. Full path created by this docker image if needed.
B2_BACKUP_KEY   | A passphrase for the backup, passed to duplicity. Also needed for restore.

Examples:

Backup:
```
#!python

docker run \
-v /opt/mystuff:/data \
-h b2backup \
-e B2_BACKUP_KEY=password123 \
-e B2_BUCKET=b2backups \
-e B2_DIR=mystuff \
-e B2_ACCOUNT_ID=1234 \
-e B2_APP_KEY=5678 \
b2backup backup
```

Restore:
```
#!python

docker run \
-v /opt/mystuff:/data \
-h b2backup \
-e B2_BACKUP_KEY=password123 \
-e B2_BUCKET=b2backups \
-e B2_DIR=mystuff \
-e B2_ACCOUNT_ID=1234 \
-e B2_APP_KEY=5678 \
b2backup restore
```

# Future #

Will consider switching to bup with encbup for global deduplication. Tarsnap is an expensive alternative.